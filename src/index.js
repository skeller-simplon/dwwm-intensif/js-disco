import $ from 'jquery';
import { BandService } from './service/band-service';

$('p').css('color', 'red')
    .addClass('myclass')
    .append('<span>test</span>')
    .on('click', () => alert('bloup'))

const bandService = new BandService();

bandService.findAll().then(bands => {
    for (let band of bands) {
        writeBand(band);
    }
}).then(response => {
    console.log(response);
});

$('#addBandForm').on('submit', function (event) {
    event.preventDefault();
    let band = {
        name: $('#bandName').val(),
        start: $('#bandStart').val(),
        end: $('#bandEnd').val(),
        country: $('#bandCountry').val()
    };
    bandService.add(band);
    writeBand(band);
});

function writeBand(band) {

    let li = $('<li>' + band.name + ' - Id : ' + band.id + '</li>').on(
        'click', () => {
            let resultat = bandService.remove(band).then(() => {
                $('body').remove(li);
            });
        });
    console.log(band);
    $('#bands-list').append(li);
}