import axios from 'axios';

export class BandService {
    constructor() {
        this.url = 'http://localhost:8000/api/band';
    };
    async findAll() {
        let response = await axios.get(this.url);
        console.log(response);
        return response.data;
    };
    add(band) {
        return axios.post(this.url, band).then(response => {
            if (response.data !== null) {
                return response.data;
            };
            return 'Error';
        });
    };
    delete(band){
        return axios.delete(this.url + '/'+ band.id).then(response => {
            if (response.data !== null){
                return response.data;
            }
            return 'Error'
        });
    };
};